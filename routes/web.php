<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebsiteController;

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::view('/', 'website.index')->name('home');
Route::view('/about', 'website.about')->name('about');
Route::get('banners-data', [WebsiteController::class, 'banners']);
Route::get('/solutions/{solution}', [WebsiteController::class, 'solution'])->name('solutions.show');
Route::view('/services', 'website.services')->name('services');
Route::view('/contact', 'website.contact')->name('contact');
Route::view('/privacy-policy', 'website.privacy')->name('privacy');
// Route::post('download-presentation', [WebsiteController::class, 'download_presentation'])->name('download-presentation');
// Route::get('products-and-services-catalog', [WebsiteController::class, 'our_corporate_presentation']);
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use Validator;

class WebsiteController extends Controller
{
    public function solution($solution){
        $view = 'website.solutions.' . $solution;
        if(view()->exists($view)){
            return view($view);
        }else{
            return abort(404);
        }
    }


    public function banners(){

        $banners = [

            [
                'title' => __('website.solutions.enterprise.title'),
                'short_desc' => __('website.solutions.enterprise.short_desc'),
                'route' => __('website.solutions.enterprise.route'),
                'icon' => __('website.solutions.enterprise.icon'),
                'image' => __('website.solutions.enterprise.image'),
            ],

            [
                'title' => __('website.solutions.data-center.title'),
                'short_desc' => __('website.solutions.data-center.short_desc'),
                'route' => __('website.solutions.data-center.route'),
                'icon' => __('website.solutions.data-center.icon'),
                'image' => __('website.solutions.data-center.image'),
            ],

            [
                'title' => __('website.solutions.its.title'),
                'short_desc' => __('website.solutions.its.short_desc'),
                'route' => __('website.solutions.its.route'),
                'icon' => __('website.solutions.its.icon'),
                'image' => __('website.solutions.its.image'),
            ],

            [
                'title' => __('website.solutions.software.title'),
                'short_desc' => __('website.solutions.software.short_desc'),
                'route' => __('website.solutions.software.route'),
                'icon' => __('website.solutions.software.icon'),
                'image' => __('website.solutions.software.image'),
            ],

            [
                'title' => __('website.solutions.electronic.title'),
                'short_desc' => __('website.solutions.electronic.short_desc'),
                'route' => __('website.solutions.electronic.route'),
                'icon' => __('website.solutions.electronic.icon'),
                'image' => __('website.solutions.electronic.image'),
            ],

            [
                'title' => __('website.solutions.cyber-security.title'),
                'short_desc' => __('website.solutions.cyber-security.short_desc'),
                'route' => __('website.solutions.cyber-security.route'),
                'icon' => __('website.solutions.cyber-security.icon'),
                'image' => __('website.solutions.cyber-security.image'),
            ],

            [
                'title' => __('website.solutions.access-and-wireless.title'),
                'short_desc' => __('website.solutions.access-and-wireless.short_desc'),
                'route' => __('website.solutions.access-and-wireless.route'),
                'icon' => __('website.solutions.access-and-wireless.icon'),
                'image' => __('website.solutions.access-and-wireless.image'),
            ],

        ];

        return $banners;

    }

    public function download_presentation(Request $request){

        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:subscribers',
            'company' => 'required|string',
        ];

        $validate = Validator::make($request->except('_token'), $rules)
            ->validate();

        $subscriber = new Subscriber();    
        $subscriber->name = $request->name;
        $subscriber->email = $request->email;
        $subscriber->company = $request->company;
        $subscriber->save();

        $presentation = public_path('files-xf2021w3/dynamic_communications_catalog_2021.pdf');

        // we need to download the presentation file
        return response()->download($presentation);

    }

    public function our_corporate_presentation(){
        $presentation = public_path('files-xf2021w3/dynamic_communications_catalog_2021.pdf');
        return response()->file($presentation);
    }

}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Subscriber;
use Validator;

class SubscribeForm extends Component
{

    public $name;
    public $email;
    public $subscribed = false;

    public function render()
    {
        return view('livewire.subscribe-form');
    }

    public function subscribe_user(){
        
        $validate = Validator::make([

            'name' => $this->name,
            'email' => $this->email

        ], [

            'name' => 'required|string',
            'email' => 'required|email|unique:subscribers'

        ], [

            'name.required' => 'Please enter your full name',
            'email.required' => 'Please enter your e-mail',
            'email.email' => 'Please enter a valid e-mail',
            'email.unique' => 'You are already registered in our newsletter'

        ])->validate();

        $subscriber = new Subscriber();
        $subscriber->name = $this->name;
        $subscriber->email = $this->email;
        $subscriber->save();

        $this->subscribed = true;

    }

}

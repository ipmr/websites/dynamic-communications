<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsiteIconsList extends Component
{
    public function render()
    {
        return view('livewire.website-icons-list');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsiteCarousel extends Component
{
    public function render()
    {
        return view('livewire.website-carousel');
    }
}

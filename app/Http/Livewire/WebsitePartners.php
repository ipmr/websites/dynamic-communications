<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsitePartners extends Component
{
    public function render()
    {
        return view('livewire.website-partners');
    }
}

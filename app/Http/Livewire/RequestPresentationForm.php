<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RequestPresentationForm extends Component
{
    public function render()
    {
        return view('livewire.request-presentation-form');
    }
}

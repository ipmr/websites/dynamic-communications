<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsiteCertifications extends Component
{
    public function render()
    {
        return view('livewire.website-certifications');
    }
}

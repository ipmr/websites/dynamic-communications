<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsiteHomeOverview extends Component
{
    public function render()
    {
        return view('livewire.website-home-overview');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WebsiteHomeServices extends Component
{
    public function render()
    {
        return view('livewire.website-home-services');
    }
}

<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SectionSplit extends Component
{

    public $title;
    public $description;
    public $pathimg;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $pathimg)
    {
        $this->title = $title;
        $this->pathimg = $pathimg;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.section-split');
    }
}

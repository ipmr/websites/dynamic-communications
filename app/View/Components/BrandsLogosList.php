<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BrandsLogosList extends Component
{
    public $logos;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($logos)
    {
        $this->logos = $logos;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.brands-logos-list');
    }
}

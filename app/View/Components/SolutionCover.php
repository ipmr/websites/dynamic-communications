<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SolutionCover extends Component
{

    public $title;
    public $description;
    public $bgimg;
    public $icon;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $bgimg, $icon)
    {
        $this->title = $title;
        $this->bgimg = $bgimg;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.solution-cover');
    }
}

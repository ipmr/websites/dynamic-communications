require('./bootstrap');
import {WOW} from 'wowjs';
import Vue from 'vue';
import SlickCarousel from './components/SlickCarousel';
import MobileBtn from './components/MobileBtn';
const app = new Vue({
	el: '#app',
	components: {SlickCarousel, MobileBtn},
	mounted(){

		this.init_animate();

	},
	methods: {
		init_animate(){
			let t = this;
			var wow = new WOW(
				{
					boxClass:     'wow',      // animated element css class (default is wow)
					animateClass: 'animate__animated', // animation css class (default is animated)
					offset:       0,          // distance to the element when triggering the animation (default is 0)
					mobile:       false,       // trigger animations on mobile devices (default is true)
					live:         false,       // act on asynchronously loaded content (default is true)
				}
			);
			wow.init();
		}
	}
});
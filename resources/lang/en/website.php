<?php

return [
    'we-install' => 'We install',
    'solutions-provided' => 'These solutions are provided by',
    'solutions' => [
        'our-solutions' => 'Our Solutions',
        'enterprise' => [
            'title' => 'Enterprise and Business Communications',
            'short_desc' => 'DYNACOM´s Enterprise and Business Communications are the most innovative on the market today, providing high interactivity among peers, at the office or around the world.',
            'route' => 'enterprise-and-business-communications',
            'icon' => 'icon_collaboration_solutions.svg',
            'image' => 'samson-ZGjbiukp_-A-unsplash.jpg',

            'connected' => [

                'title' => 'Connected synergetic group',
                'description' => 'Meet, join, work, congregate, collaborate just with a touch of a button, seamlessly sharing your point of view and experiences within your respective colleagues in an always on connected synergetic group.',

            ],

            'telephony' => [

                'title' => 'IP Telephony',
                'description' => '<b>Dynamic Communications</b> offers IP Telephony Solutions that support advanced, reliable, voice communication, across your business infrastructure allowing the best integration with your operations, reducing overall communication cost among peers, improving productivity and optimizing time.'

            ],

            'unified' => [

                'title' => 'Unified Collaboration',
                'description' => 'The open architecture of today´s communication solutions allow a broader access to different kinds of tools that allow your employees to enhance their productivity even further, with unified communications you can be in touch with anyone in your company no matter which communication method they are using, whether by voice, message, SMS, email, chat they become available to your any time anywhere.'

            ],

            'mobility' => [

                'title' => 'Mobility Solutions',
                'description' => '<p class="lead text-white">Ever thought of changing the business environment for your mobile employees, giving them the freedom to communicate from wherever they are, with any device, even with one brought by their own (BYOD), on a system seamlessly integrated to your core communications solution</p>

                <p class="lead text-white">Mobility solutions can help you accomplish that, without the need of even changing your current internal communication solution.</p>'

            ],

            'video' => [

                'title' => 'HD Video Presence',
                'description' => 'Get close and personal at your business meetings around the globe without leaving your office, HD Video Presence brings you a new level of interaction with your colleagues, business partners and customers without the necessity of travel, reducing overall cost, saving you travel time, and improving your business productivity.'

            ],

            'apps' => [

                'title' => 'Business Apps',
                'description' => "You already invested in state-of-the-art technology for your communication solutions but you still have issues to leverage all those services together?; we can help you build the correct application for your business, no matter if it's a multi vendor, multicarrier or multi platform solution we can manage your solution with the finality of helping you grow."

            ]

        ],
        'data-center' => [
            'title' => 'Data Center',
            'short_desc' => 'Your network´s systems are a key component of your company’s operations, this is why it’s very important to have your systems at optimal speed.',
            'route' => 'data-center',
            'icon' => 'icon_data_center_solutions.svg',
            'image' => 'taylor-vick-M5tzZtFCOfs-unsplash.jpg',

            'installation' => [

                'title' => 'INSTALLATION AND IMPLEMENTATION',
                'description' => 'It doesn’t matter if you have just a few computers in your network, an small site or a big infrastructure in your data center full of mission critical services, we are able to help you to tackle any installation, implementation, restructuration or enhancement.'

            ],

            'routing' => [

                'title' => 'ROUTING AND SWITCHING',
                'description' => "Today's challenge is to keep up with your business´s speed in terms of network traffic, with the incursion of newest technologies, Web 2.0, Video Collaboration services, and social media, it's been an interesting time for IT managers, that's why Dynamic Communications have the most advanced Routing and Switching solutions on the market."

            ],

            'server' => [

                'title' => 'SERVERS AND STORAGE',
                'description' => '

                <p class="lead mb-7 text-white">
                    Did you know that something as important as your customers is inside your Data Center? Yes we are talking about the servers and storage; imagine that you lose all of your CRM databases? Imagine that you can\'t send emails to your customers? What would be the result of that? It might sound so scary that may not want to even think of that right?.
                </p>
                <p class="lead text-white">
                    This is why those systems are so important, but don´t worry our solutions are the most thrust wordy on the market, more than 100 years on the business avail them. With the integrated solutions that Dynamic Communications bring you all your company\'s belongings are in safe hands. 
                </p>
                
                '

            ],

            'environmental' => [

                'title' => 'ENVIRONMENTAL AND ENERGY PROTECTION',
                'description' => 'Great you have a site/Data Center, is it in a safe environment? The electrical energy that your systems are being supplied with is intelligently controlled? Do you have problems with the site´s humidity, temperature or any unauthorized intrusion? Dynamic Communications has an answer for every question with an advanced solution portfolio that can be tailored for your business needs.'

            ]

        ],
        'its' => [
            'title' => 'ITS',
            'short_desc' => 'We understand that today’s activities rely completely on Information Technologies which are sustained in your cabling infrastructure, but is your structured cabling designed to hold all of your network´s traffic?.',
            'route' => 'its',
            'icon' => 'icon_its.svg',
            'image' => 'electrical_cabling_and_network2_sml.jpg',

            'copper' => [

                'title' => 'Copper Cabling Solutions',
                'description' => 'We bring the most reliable cabling solutions on the market as we implement them using the world’s best vendors, offering scalable, modular and certified cabling using the best practices to ensure your network´s infrastructure optimal performance all the time. Our Copper Cabling solutions range from these 3 different categories:',

                'categories' => [

                    'cat5e' => [
                        'title' => 'CAT5E',
                        'description' => 'The most popular cabling category, this cable can support the following speeds, 10MB/100MB/1000MB at three levels of shielding, UTP, FTP and SFTP.'
                    ],

                    'cat6' => [

                        'title' => 'CAT6',
                        'description' => "This type of cabling solution is becoming very popular among customers with high traffic on their networks and it's recommended for IP telephony implementation, this solution runs at natively at GigaSpeed (1000MB) at three levels of shielding – UTP, FTP and SFTP."

                    ],

                    'cat6a' => [

                        'title' => 'CAT6A',
                        'description' => 'This solution is recommended for Ultra High Network traffic or Data Center Environments this cable speed is Ten Gigaspeed (10,000MB), these cables are shielded in the following standards SFTP, FFTP and FUTP.'

                    ]

                ]

            ],

            'fiber' => [

                'title' => 'FIBER OPTIC',
                'description' => '
                    <p class="lead text-white">
                        High demand data transfers require bringing up the speed of your network to a maximum, as well as data centers and riser backbones, for these particular scenarios is always recommended to implement Dynamic Communications’ fiber optics structured cabling solutions which take advantage of the latest industries ‘standards to enhance your network´s connectivity to its optimal functionally.
                    </p>
                
                    <p class="lead text-white font-bold">
                        We offer fiber optic state-of-the-art solutions and:
                    </p>

                    <ul class="flex flex-col space-y-5 text-red-400 font-bold">
                        <li>Cabling solution pre-configuration, installation, reconfiguration and maintenance.</li>
                        <li>On site Fiber Optic Fusion.</li>
                        <li>Riser Backbone optimization.</li>
                    </ul>
                '

            ]
        ],
        'software' => [
            'title' => 'Software',
            'short_desc' => 'Our partners from IP Media River are a team of designers, webmasters and app developers that can help you build your new corporate productivity tools from the ground up.',
            'route' => 'software',
            'icon' => 'icon_software_solutions.svg',
            'image' => 'fotis-fotopoulos-DuHKoV44prg-unsplash.jpg',

            'increase' => [

                'title' => 'INCREASE YOUR PRODUCTIVITY',
                'description' => 'Solutions based on the cloud all of your employees will be able to access the critical information any time anywhere from any web enabled device maintaining top levels of productivity'

            ],

            'web-development' => [

                'title' => 'WEB DEVELOPMENT AND GRAPHIC DESIGN',
                'description' => '
                    <h1 class="text-xl font-bold uppercase mb-7">Web Development</h1>
                    <p class="lead">Need a tailored productivity solution, we can help you accomplish your goals with our web and software developing team that understands your needs and knows how to translate those requirements in a business enhancing application for your company stored in the cloud.</p>
                    <h1 class="text-xl font-bold uppercase mb-7">Graphic Design</h1>
                    <p class="lead">The identity of a company is its image, the main idea that stays with the consumer, acting as one of the main sales factors, this is why we partner with IP media river to deliver quality design to your company always having in mind the latest consumer design trends to ensure that your image stays fresh several years from now.</p>
                '

            ],

            'apps' => [

                'title' => 'BUSINESS APPS',
                'description' => "Need a CRM or an ERP application for your company? We’ve got you covered, are you in need of a tailored solution that you can't find nowhere else? Don´t look any further, we have a team of specialized programmers that can help you leverage an integrated solution made just for you."

            ],

            'licensing' => [

                'title' => 'LICENSING',
                'description' => '

                    <p class="lead text-white">We have partnered with the industry\'s leader in software and operational systems, yes Microsoft, we deliver high quality solutions that will be constantly updated to ensure optimal productivity all the time, without risking compatibility on the process, our licensing solutions are:</p>
                    <ul class="font-bold flex flex-col space-y-5 text-white mb-7">
                        <li>Operating systems for PC and servers</li>
                        <li>Productivity software such as office</li>
                    </ul>

                '

            ]
 
        ],
        'electronic' => [
            'title' => 'Electronic Security',
            'short_desc' => 'Nowadays security is no longer a choice but a necessity, our security solutions offer the peace of mind of knowing that your business is always monitored with our without guards on duty.',
            'route' => 'electronic-security',
            'icon' => 'icon_electronic_security.svg',
            'image' => 'christina-wocintechchat-com-faEfWCdOKIg-unsplash.jpg',

            'cctv' => [

                'title' => 'CCTV',
                'description' => 'We incorporate state-of-the-art video surveillance systems costumed for your security requirements, our solutions can record and stream High-quality HD video over IP connections making them a powerful tool against any situation you may stumble with.'

            ],

            'access' => [

                'title' => 'ACCESS AND PERIMETER CONTROL',
                'description' => "Yes you wish to grant access to your employees but what it's the level of clearance they have within your business perimeter? Are they allowed to wander around between departments? Do you have critical systems and information located in your site? You can help resolve all of these questions by implementing an advanced access and perimeter control system that will allow only authorized access to the correct area any time."

            ],



        ],
        'cyber-security' => [
            'title' => 'Cyber Security',
            'short_desc' => 'Networks nowadays need to be secure against malicious software, viruses and most importantly hackers.',
            'route' => 'cyber-security',
            'icon' => 'icon_network_solutions.svg',
            'image' => 'chris-yang-1tnS_BVy9Jk-unsplash.jpg',

            'threat' => [

                'title' => 'Threat and Intrusion',
                'description' => 'These are hard times for your IT managers in terms of network security, they are having trouble figuring which is the best IT security solution that will be able to protect your network´s systems from unauthorized entry whether external or internal, this is why we have condensed the solution portfolio to accomplish that goal, making it easier to your IT department.'

            ],

            'firewall' => [

                'title' => 'FIREWALL',
                'description' => 'These systems are mandatory in today´s site and data center environments in which threats are an everyday problem, these filters or firewalls as they are commonly known are responsible of managing inbound as outbound network traffic to secure any vulnerability that your network may have, some firewalls can even adapt to the threat response making it harder to the attacker, stopping the attack from even starting.'

            ],

            'web-security' => [

                'title' => 'Web Security',
                'description' => 'Your website and all your internet enabled systems are in a potential threat, sometimes even the strongest security systems and firewalls can be fooled and bypassed but not with our advanced and reliable web security tools, trust in the solutions that Dynamic Communications bring you.'

            ],


            'intelligent' => [

                'title' => 'INTELLIGENT BUSINESS PROTECTION SYSTEMS',
                'description' => "When regular security systems are not enough to secure your business holdings, stakeholders can be at risk of losing valuable patrimonial or infrastructural belongings among other things; this is why Dynamic Communications can offer intelligent Business Protection systems to ensure your company's safety all the time.",
                'list' => '
                    <p class="subtitle text-white wow animate__fadeInUp">Our intelligent Business Protection systems include:</p>
                    <ul class="text-white font-bold flex flex-col space-y-4 mb-7">
                        <li>Advanced Video Analytics</li>
                        <li>RFID Tags and Survey</li>
                        <li>Netbotz</li>
                        <li>Fire and alarm system automatization.</li>
                    </ul>
                '

            ]
        ],
        'access-and-wireless' => [
            'title' => 'Access and Wireless',
            'short_desc' => 'Knowing the correct wireless network and speed is very relevant when the implementation plan is at early stages and it’s key to your business operations in the future.',
            'route' => 'access-and-wireless',
            'icon' => 'icon_access_and_wireless.svg',
            'image' => 'christina-wocintechchat-com-LDap559l-MU-unsplash.jpg',

            'access' => [

                'title' => 'ACCESS POINTS AND BRIDGES',
                'description' => 'Deliver only the best wireless business infrastructure solutions to your company; we do a thorough exam to your wireless needs to implement the right solution for your business. We also provide site surveys to investigate if your wireless network problems rely on other wireless network’s interference.',

                'access-point' => [

                    'title' => 'ACCESS POINTS',
                    'description' => 'Utilizing the best single, dual and multi Band wireless solutions we can accommodate your wireless infrastructure to couple with your current wireless devices ensuring complete compatibility and maximum network speed.'

                ],

                'wireless' => [

                    'title' => 'WIRELESS BRIDGES',
                    'description' => 'Some times when a wireless access point doesn’t reach certain locations within your business perimeter a wireless bridge or repeater can be implemented to increase your wireless range, enhancing your wireless coverage and keeping all your wireless users happy.'                    

                ]

            ],

            'wireless-campus' => [

                'title' => 'WIRELESS CAMPUS INFRASTRUCTURE (MESH)',
                'description' => 'If you require delivering wireless access to a big campus, bigger solutions are always recommended, this is when Wireless Mesh comes in to play, and this is wireless interconnected network with several points within a perimeter allowing a much more wider access to wireless connectivity, this solution is recommended for schools, malls, theme parks, government buildings, Industrial complexes, shipyards and manufacture companies that have to provide wireless access to a really big area.'

            ],

            'wimax' => [

                'title' => 'WiMAX',
                'description' => "It's your business perimeter so large that requires an unparalleled solution? If this is the case we introduce WiMAX (802.16b standard) a solution that runs on a different wireless frequency (3.5GHz and 5.8GHz) designed to reach longer distances than regular WiFi, this solution is required for MAN (Metropolitan Area Network) or WAN (Wide Area Network) infrastructures, it can also be implemented for rural or desert areas were building a cabled infrastructure can be very expensive."

            ]

        ]
    ],
    'certifications' => [

        'title' => 'OUR CERTIFICATIONS',
        'description' => 'Part of why customers prefer our services instead of our competition are our certifications as you may know IT oriented companies and solutions require certification for their implementation, this is why we are proud to present you some of our certifications.'

    ]

];

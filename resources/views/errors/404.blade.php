<x-page-layout>
    <section class="py-32">
        <div class="website-container">
            <h1 class="h3 text-center">Oops! Error 404</h1>
            <p class="text-2xl text-center">We can't seem to find the page you're looking for.</p>
        </div>
    </section>
</x-page-layout>

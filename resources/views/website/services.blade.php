<x-guest-layout>
    @section('page-title', '| Smart-D Services')
    <!-- Cover -->
    <x-services-cover title="SMART-D SERVICES" bgimg="/img/unsplash/krakenimages-376KN_ISplE-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                Smart D services change the way you think about IT solutions, gathering, implementing and enhancing your network infrastructure to its full potential, helping you unleash the power of true collaboration environments, empowering your IT tools and increasing your company's productivity exponentially. 
            </p>
        </x-slot>
    </x-services-cover>
    <!-- IT/CLOUD CONSULTING (DYNACOM ONE APPROACH) -->
    <div id="it-consulting">
        <x-section-split title="IT/CLOUD CONSULTING (DYNACOM ONE APPROACH)" pathimg="/img/unsplash/dane-deaner-iJ1lw8iNIy8-unsplash.jpg">
            <x-slot name="description">
                <p class="lead">Our Team of specialized consulters will advise you in the latest technological trends and solutions, they can assist you ascertain which technology is best for you, based on the world´s best practices and case studies.</p>

                <p class="lead">When you interact with one of our highly skilled consulters, he/she will listen to your needs, catering them as a valued member of your internal IT Staff. Remember we are here to help you; our ONE Account Managers will lead you to the resolution of all your IT needs.</p>
            </x-slot>
        </x-section-split>
    </div>

    <!-- MAINTENANCE CONTRACTS & SERVICE POLICIES -->
    <div id="maintenance">
        <x-section-split-inverse title="MAINTENANCE CONTRACTS & SERVICE POLICIES" pathimg="/img/unsplash/razvan-chisu-Ua-agENjmI4-unsplash.jpg">
            <x-slot name="description">
                <p class="lead">We understand that you require business continuity to lead your respective market, and this is why we developed our maintenance contracts so you can rest easy and let us take care of all your IT equipment for you, ensuring optimal functionality all the time. We have established the policy that is right for you.</p>
            </x-slot>
        </x-section-split-inverse>
    </div>

    <!-- NETWORK ASSESSMENT (LAN, WAN AND WIFI) -->
    <div id="network-assessment">
        <x-section-bg-fixed title="NETWORK ASSESSMENT (LAN, WAN AND WIFI)" pathimg="/img/unsplash/fluke_networks.jpg">
            <x-slot name="description">
                <p class="lead text-white">Have you experienced bottlenecks or slowdowns in your network and you don't know why? We can offer a network assessment for your system´s infrastructure to validate if it is running smoothly or not, most of the time network slowdown is due configuration errors that can be easily fixed or just wrong cabling/hotspot distribution.</p>
            </x-slot>
        </x-section-bg-fixed>
    </div>

    <!-- IT EQUIPMENT CONFIGURATION AND INSTALLATION -->
    <div id="it-equipment">
        <x-section-solid-and-overlay title="IT EQUIPMENT CONFIGURATION AND INSTALLATION" bgimg="/img/unsplash/nathan-dumlao-kEOLkJksbc8-unsplash.jpg">
            <x-slot name="description">
                <p class="lead text-white">Need help installing or configuring your IT equipment, we can help you with that, our team of engineers are always ready to help no matter the issue at hand, from simply setting up a mail server to installing and configuring a blade server system we have the knowledge to assist you with every IT need.</p>
            </x-slot>
        </x-section-solid-and-overlay>
    </div>

    <!-- HELP DESK -->
    <div id="help-desk">
        <x-section-split id="helpdesk" title="HELP DESK" pathimg="/img/unsplash/arlington-research-Kz8nHVg_tGI-unsplash.jpg">
            <x-slot name="description">
                <p class="lead">Need to configure, reconfigure or update all your computers in your company but you lack of an IT department, need to install a network printer for your employees, we have all the right solutions for that, our specialized tech support team can resolve those issues for you, so now can attend what matters most - your customers.</p>
            </x-slot>
        </x-section-split>
    </div>
</x-guest-layout>
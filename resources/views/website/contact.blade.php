<x-guest-layout>
    @section('page-title', '| Contact Us')
    <!-- CONTACT US -->
    <section class="bg-overlay py-10 lg:py-20 shadow md:bg-fixed" 
        style="background-image: url('/img/unsplash/sean-pollock-PhYq704ffdA-unsplash.jpg')">
        <div class="website-container">
            <div class="flex items-center space-x-20">
                <div class="w-full md:w-2/3 text-white mx-auto text-left md:text-center">
                    <h1 class="text-6xl font-bold uppercase wow animate__fadeInUp">Contact Us</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white py-24">
        <div class="website-container flex items-center">
            <div class="w-2/3">
                <p><b>Dynacom MX</b> (664) 622-7030</p>
                <p><b>Dynacom U.S</b> (619) 616-2850</p>
                <p><b>Email:</b> info@godynacom.com</p>
            </div>
        </div>
    </section>
</x-guest-layout>
<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.electronic.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.electronic.title') }}"
        bgimg="{{ asset('/img/unsplash/' . __('website.solutions.electronic.image')) }}"
        icon="{{ asset('/img/svg/' . __('website.solutions.electronic.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
            @lang('website.solutions.electronic.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- CCTV -->
    <x-section-split title="{{ __('website.solutions.electronic.cctv.title') }}" pathimg="/img/unsplash/pawel-czerwinski-OfwiURcZwYw-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.electronic.cctv.description')
            </p>
        </x-slot>
    </x-section-split>
    <!-- Access and Perimeter Control -->
    <x-section-bg-fixed title="{{ __('website.solutions.electronic.access.title') }}" pathimg="/img/unsplash/fingerprinta.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                @lang('website.solutions.electronic.access.description')
            </p>
        </x-slot>
    </x-section-bg-fixed>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['honeywell.png', 'schneider.jpg', 'axis.png']" />
        </div>
    </section>
</x-page-layout>
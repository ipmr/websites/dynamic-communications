<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.data-center.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.data-center.title') }}"
        bgimg="{{ asset('/img/unsplash/' . __('website.solutions.data-center.image')) }}"
        icon="{{ asset('/img/svg/' . __('website.solutions.data-center.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
            @lang('website.solutions.data-center.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- Installation & Implementation -->
    <x-section-split title="{{ __('website.solutions.data-center.installation.title') }}" pathimg="/img/unsplash/jordan-harrison-40XgDxBfYXM-unsplash.jpg">
        <x-slot name="description">
            <p class="text-xl m-0">
                @lang('website.solutions.data-center.installation.description')
            </p>
        </x-slot>
    </x-section-split>
    <!-- Routing and Switching -->
    <x-section-split-inverse title="{{ __('website.solutions.data-center.routing.title') }}" pathimg="/img/unsplash/imgix-klWUhr-wPJ8-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.data-center.routing.description')
            </p>
        </x-slot>
    </x-section-split-inverse>
    <!-- Servers and Storage -->
    <x-section-bg-fixed title="{{ __('website.solutions.data-center.server.title') }}" pathimg="/img/unsplash/ian-battaglia-9drS5E_Rguc-unsplash.jpg">
        <x-slot name="description">
            {!! __('website.solutions.data-center.server.description') !!}
        </x-slot>
    </x-section-bg-fixed>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['cisco.jpg']" />
        </div>
    </section>
</x-page-layout>
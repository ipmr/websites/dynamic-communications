<x-page-layout>
    @section('page-title', '| ' .  __('website.solutions.enterprise.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.enterprise.title') }}"
        bgimg="/img/unsplash/{{ __('website.solutions.enterprise.image') }}"
        icon="/img/svg/{{ __('website.solutions.enterprise.icon') }}">
        <x-slot name="description">
            <p class="text-xl">{{ __('website.solutions.enterprise.short_desc') }}</p>
        </x-slot>
    </x-solution-cover>
    <!-- Connected synergetic group -->
    <x-section-split 
        title="{{ __('website.solutions.enterprise.connected.title') }}" 
        pathimg="/img/unsplash/mike-kononov-lFv0V3_2H6s-unsplash.jpg"> 
        <x-slot name="description">
            <p class="text-xl m-0">
            {!! __('website.solutions.enterprise.connected.description') !!}
            </p>
        </x-slot>
    </x-section-split>
    <!-- IP Telephony -->
    <x-section-split-inverse 
        title="{{ __('website.solutions.enterprise.telephony.title') }}"
        pathimg="/img/unsplash/arlington-research-kN_kViDchA0-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                {!! __('website.solutions.enterprise.telephony.description') !!}
            </p>
        </x-slot>
    </x-section-split-inverse>
    <!-- Unified Collaboration -->
    <x-section-bg-fixed 
        title="{{ __('website.solutions.enterprise.unified.title') }}" 
        pathimg="/img/unsplash/annie-spratt-QckxruozjRg-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
            {!! __('website.solutions.enterprise.unified.description') !!}
            </p>
        </x-slot>
    </x-section-bg-fixed>
    <!-- Mobility Solutions -->
    <x-section-solid-and-overlay 
        title="{{ __('website.solutions.enterprise.mobility.title') }}" 
        bgimg="/img/unsplash/christina-wocintechchat-com-LDap559l-MU-unsplash.jpg">
        <x-slot name="description">
            {!! __('website.solutions.enterprise.mobility.description') !!}
        </x-slot>
        <x-slot name="bgdescription">
            
        </x-slot>
    </x-section-solid-and-overlay>
    <!-- HD Video Presence -->
    <x-section-split-inverse 
        title="{{ __('website.solutions.enterprise.video.title') }}"
        pathimg="/img/unsplash/benjamin-child-0sT9YhNgSEs-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
            {{ __('website.solutions.enterprise.video.description') }}
            </p>
        </x-slot>
    </x-section-split-inverse>
    <!-- Business App -->
    <x-section-bg-fixed 
        title="{{ __('website.solutions.enterprise.apps.title') }}" 
        pathimg="/img/unsplash/fotis-fotopoulos-DuHKoV44prg-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
            {{ __('website.solutions.enterprise.apps.description') }}
            </p>
        </x-slot>
    </x-section-bg-fixed>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['avaya.jpg', 'cisco.jpg', 'poly.jpg', 'jabra.png', 'ipmediariver.png']" />
        </div>
    </section>
</x-page-layout>
<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.cyber-security.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.cyber-security.title') }}"
        bgimg="{{ asset('/img/unsplash/' . __('website.solutions.cyber-security.image')) }}"
        icon="{{ asset('/img/svg/' . __('website.solutions.cyber-security.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
                @lang('website.solutions.cyber-security.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- Threat and Intrusion -->
    <x-section-split title="{{ __('website.solutions.cyber-security.threat.title') }}" pathimg="/img/unsplash/mika-baumeister-Wpnoqo2plFA-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.cyber-security.threat.description')
            </p>
        </x-slot>
    </x-section-split>
    <!-- Firewall -->
    <x-section-split-inverse title="{{ __('website.solutions.cyber-security.firewall.title') }}" pathimg="/img/unsplash/philipp-katzenberger-iIJrUoeRoCQ-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.cyber-security.firewall.description')
            </p>
        </x-slot>
    </x-section-split-inverse>
    <!-- Web Security -->
    <x-section-bg-fixed title="{{ __('website.solutions.cyber-security.web-security.title') }}" pathimg="/img/unsplash/andrew-neel-ute2XAFQU2I-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                @lang('website.solutions.cyber-security.web-security.description')
            </p>
        </x-slot>
    </x-section-bg-fixed>
    <!-- Intelligent Business Protection Systems -->
    <x-section-solid-and-overlay title="{{ __('website.solutions.cyber-security.intelligent.title') }}" bgimg="/img/unsplash/2016.05.07-RFID-1280x640.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                @lang('website.solutions.cyber-security.intelligent.description')
            </p>
        </x-slot>
        <x-slot name="bgdescription">
            {{ __('website.solutions.cyber-security.intelligent.list') }}
        </x-slot>
    </x-section-solid-and-overlay>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['cisco.jpg', 'meraki.png', 'checkpoint.png']" />
        </div>
    </section>
</x-page-layout>
<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.software.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.software.title') }}"
        bgimg="{{ asset('/img/unsplash/' . __('website.solutions.software.image')) }}"
        icon="{{ asset('/img/svg/' . __('website.solutions.software.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
            @lang('website.solutions.software.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- General Description -->
    <x-section-split title="{{ __('website.solutions.software.increase.title') }}" pathimg="/img/unsplash/carlos-muza-hpjSkU2UYSU-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.software.increase.description')
            </p>
        </x-slot>
    </x-section-split>
    <!-- Web design & development -->
    <x-section-split-inverse title="{{ __('website.solutions.software.web-development.title') }}" pathimg="/img/unsplash/sigmund-4UGmm3WRUoQ-unsplash.jpg">
        <x-slot name="description">
            {!! __('website.solutions.software.web-development.description') !!}
        </x-slot>
    </x-section-split-inverse>
    <!-- Business Apps -->
    <x-section-solid-and-overlay title="{{ __('website.solutions.software.apps.title') }}" bgimg="/img/unsplash/fatos-bytyqi-Agx5_TLsIf4-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                @lang('website.solutions.software.apps.description')
            </p>
        </x-slot>
        <x-slot name="bgdescription"></x-slot>
    </x-section-solid-and-overlay>
    <!-- Licensing -->
    <x-section-bg-fixed title="{{ __('website.solutions.software.licensing.title') }}" pathimg="/img/unsplash/freestocks-BStW5kYXw4E-unsplash.jpg">
        <x-slot name="description">
            {!! __('website.solutions.software.licensing.description') !!}
        </x-slot>
    </x-section-bg-fixed>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['ipmediariver.png', 'microsoft.png']" />
        </div>
    </section>
</x-page-layout>
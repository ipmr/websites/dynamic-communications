<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.its.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.its.title') }}"
        bgimg="{{ asset('img/unsplash/' . __('website.solutions.its.image')) }}"
        icon="{{ asset('img/svg/' . __('website.solutions.its.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
                @lang('website.solutions.its.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- Copper Cabling Solutions -->
    <x-section-split title="{{ __('website.solutions.its.copper.title') }}" pathimg="/img/unsplash/imgix-tebFqdANuxs-unsplash.jpg">
        <x-slot name="description">
            <p class="text-xl m-0">
            @lang('website.solutions.its.copper.description')
            </p>
        </x-slot>
    </x-section-split>
    <section class="py-10 bg-dark-900 text-white">
        <div class="website-container">
            <ul class="block md:flex items-start md:space-x-20">
                <li class="flex-1 mb-7 md:mb-0">
                    <p class="text-xl text-red-500 font-bold uppercase mb-7 wow animate__fadeInUp" data-wow-delay=".3s">
                        @lang('website.solutions.its.copper.categories.cat5e.title')
                    </p>
                    <p>
                        @lang('website.solutions.its.copper.categories.cat5e.description')
                    </p>
                </li>

                <li class="flex-1 mb-7 md:mb-0">
                    <p class="text-xl text-red-500 font-bold uppercase mb-7 wow animate__fadeInUp" data-wow-delay=".3s">
                        @lang('website.solutions.its.copper.categories.cat6.title')
                    </p>
                    <p>
                        @lang('website.solutions.its.copper.categories.cat6.description')
                    </p>
                </li>

                <li class="flex-1 mb-7 md:mb-0">
                    <p class="text-xl text-red-500 font-bold uppercase mb-7 wow animate__fadeInUp" data-wow-delay=".3s">
                        @lang('website.solutions.its.copper.categories.cat6a.title')
                    </p>
                    <p>
                        @lang('website.solutions.its.copper.categories.cat6a.description')
                    </p>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fiber Optic -->
    <x-section-bg-fixed title="{{ __('website.solutions.its.fiber.title') }}" pathimg="/img/unsplash/jj-ying-8bghKxNU1j0-unsplash.jpg">
        <x-slot name="description">
            {!! __('website.solutions.its.fiber.description') !!}
        </x-slot>
    </x-section-bg-fixed>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['panduit.jpg', 'commscope.jpg', 'uniprise.png', 'icc.png', 'cpi.png']" />
        </div>
    </section>
</x-page-layout>
<x-page-layout>
    @section('page-title', '| ' . __('website.solutions.access-and-wireless.title'))
    <!-- Cover -->
    <x-solution-cover 
        title="{{ __('website.solutions.access-and-wireless.title') }}"
        bgimg="{{ asset('/img/unsplash/' . __('website.solutions.access-and-wireless.image')) }}"
        icon="{{ asset('/img/svg/' . __('website.solutions.access-and-wireless.icon')) }}">
        <x-slot name="description">
            <p class="text-xl">
                @lang('website.solutions.access-and-wireless.short_desc')
            </p>
        </x-slot>
    </x-solution-cover>
    <!-- Access Points & Bridges -->
    <x-section-split 
        title="{{ __('website.solutions.access-and-wireless.access.title') }}" 
        pathimg="/img/unsplash/glenn-carstens-peters-npxXWgQ33ZQ-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
            @lang('website.solutions.access-and-wireless.access.description')
            </p>
        </x-slot>
    </x-section-split>
    <section class="bg-dark-900 py-10">
        <div class="website-container block md:flex items-start md:space-x-24">
            <div class="flex-1">
                <p class="text-xl text-red-500 font-bold uppercase mb-7 wow animate__fadeInUp" data-wow-delay=".3s">
                    @lang('website.solutions.access-and-wireless.access.access-point.title')
                </p>
                <p class="lead text-white">
                    @lang('website.solutions.access-and-wireless.access.access-point.description')
                </p>
            </div>
            <div class="flex-1 mt-10 md:mt-0">
                <p class="text-xl text-red-500 font-bold uppercase mb-7 wow animate__fadeInUp" daa-wow-delay=".6s">
                    @lang('website.solutions.access-and-wireless.access.wireless.title')
                </p>
                <p class="lead text-white">
                    @lang('website.solutions.access-and-wireless.access.wireless.description')
                </p>
            </div>
        </div>
    </section>
    <!-- Wireless Campus Infrastructure (Mesh) -->
    <x-section-split-inverse title="{{ __('website.solutions.access-and-wireless.wireless-campus.title') }}" pathimg="/img/unsplash/walling-UP7JSnodG2M-unsplash.jpg">
        <x-slot name="description">
            <p class="lead">
                @lang('website.solutions.access-and-wireless.wireless-campus.description')
            </p>
        </x-slot>
    </x-section-split-inverse>
    <!-- WiMAX -->
    <x-section-solid-and-overlay title="{{ __('website.solutions.access-and-wireless.wimax.title') }}" bgimg="/img/unsplash/tyler-franta-RbFDzMKTH6Q-unsplash.jpg">
        <x-slot name="description">
            <p class="lead text-white">
                @lang('website.solutions.access-and-wireless.wimax.description')
            </p>
        </x-slot>
    </x-section-solid-and-overlay>
    <!-- Brands -->
    <section class="py-15 bg-white">
        <div class="website-container">
            <h1 class="text-2xl font-bold uppercase mb-7 text-center">{{ __('website.solutions-provided') }}:</h1>
            <x-brands-logos-list :logos="['cisco.jpg', 'meraki.png', 'ruckus.jpg']" />
        </div>
    </section>
</x-page-layout>
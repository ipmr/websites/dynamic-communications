<x-page-layout>
    @section('page-title', '| About Us')
    <!-- OVERVIEW -->
    <section class="bg-overlay py-10 lg:py-40 shadow md:bg-fixed" style="background-image: url('/img/unsplash/sean-pollock-PhYq704ffdA-unsplash.jpg')">
        <div class="website-container">
            <div class="flex items-center space-x-20">
                <div class="w-full md:w-2/3 text-white mx-auto text-left md:text-center">
                    <h3 class="text-lg font-bold uppercase opacity-50">About Us</h3>
                    <h1 class="slick-title uppercase wow animate__fadeInUp">Overview</h1>
                    <p class="text-base md:text-xl lg:text-2xl">
                    For now more than {{ config('app.years_of_experience') }} years, <b>Dynamic Communications</b> has been an industry leader in Seamless International IP Telephony Applications, Data Networks and Structured Cabling Solutions. Our customers range from Small Businesses to Large Multinational Companies, across a variety of Vertical Markets.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- HISTORY -->
    <section class="py-10 lg:py-20" id="history">
        <div class="website-container">
            <div class="bg-danger block lg:flex lg:items-stretch rounded shadow-xl overflow-hidden">
                <div class="w-full lg:w-1/2 p-7 lg:p-15">
                    <h1 class="h3 wow animate__fadeInUp text-white" data-wow-delay=".5s">
                        History
                    </h1>

                    <p class="mb-4 text-white">
                    <b>Dynamic Communications</b> was founded on Q3 1998, and got fully operational on Q1 1999 under the visionary and innovative idea of bringing <b>Dynamic Communications</b> Solutions to all SME and Manufacturing industries on the northwestern region of Mexico; When operations started our company was 100% focused on bringing integrated telecommunications services, high quality IT equipment and the best customer oriented care to all businesses in our region and we keep that oath today.
                    </p>

                    <p class="mb-4 text-white">
                    As business grew our operations grew as well then we realized that we needed to expand to other parts of Mexico like Mexicali, Hermosillo, Nogales, CD Juarez, Chihuahua, La Paz among others; after establishing our company at these other regions we positioned ourselves as the leader provider of telecommunication and IT solutions, the following years we dedicated our time and effort to exceed our customers’ expectations by delivering compelling and state-of-the-art solutions helping our customer on becoming leaders their respective markets.
                    </p>
                </div>
                <div class="w-full h-auto md:h-96 lg:h-auto lg:w-1/2 bg-overlay p-7 lg:p-15" style="background-image: url('/img/unsplash/bruce-mars-xj8qrWvuOEs-unsplash.jpg')">
                    <div class="relative z-10 h-full w-full flex flex-col">
                        <p class="text-white text-xl md:text-3xl font-bold uppercase leading-tight md:leading-normal mb-15">
                            We welcome you on this exciting history as we help you achieve your goals.
                        </p>
                        <div class="opacity-25 mt-auto">
                            <img src="/img/svg/logo_dynacom_xs.svg" class="w-32 lg:w-48 h-auto wow animate__bounceInUp" data-wow-delay=".25s" data-wow-duration="1.5s" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- PRESENCE -->
    <section class="bg-dark-900 py-10 lg:py-32" id="presence">
        <div class="website-container">
            <div class="flex">
                <div class="w-full lg:w-3/4 mx-auto block md:flex items-center space-x-0 md:space-x-24">
                    <div class="w-full md:w-1/2 px-4 md:px-10 mb-15 md:mb-0">
                        <img src="/img/svg/map_dot.svg" class="w-full h-auto" alt="">
                    </div>    
                    <div class="w-full text-center md:text-left md:w-1/3">
                        <h1 class="h3 text-white wow animate__fadeInUp">PRESENCE</h1>
                        <ul class="text-sm lg:text-xl text-white font-bold space-y-2 uppercase">
                            <li class="wow animate__fadeInRight" data-wow-delay=".1s" data-wow-duration="1.5s">Tijuana</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".2s" data-wow-duration="1.5s">Mexicali</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".3s" data-wow-duration="1.5s">La Paz</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".4s" data-wow-duration="1.5s">Nogales</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".5s" data-wow-duration="1.5s">Hermosillo</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".6s" data-wow-duration="1.5s">Cd. Juárez</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".7s" data-wow-duration="1.5s">Chihuahua</li>
                            <li class="wow animate__fadeInRight" data-wow-delay=".8s" data-wow-duration="1.5s">San Luis Potosí</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @livewire('website-partners')
</x-page-layout>
<x-guest-layout>
	@section('page-title', '| Welcome')
	@livewire('website-carousel')
	@livewire('website-icons-list')
	@livewire('website-home-overview')
	@livewire('website-home-services')	
	@livewire('website-certifications')
	@livewire('request-presentation-form')
</x-guest-layout>
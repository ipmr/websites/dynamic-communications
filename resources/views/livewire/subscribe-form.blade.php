<div>
    <h2 class="text-lg font-bold uppercase mb-7">
        Subscribe to our newsletter
    </h2>
    @if(!$subscribed)
        <p>Join our subscribers list to get the latest news, updates and special offers delivery directly in your inbox.</p>
        <form class="mt-7">
            <div class="form-group">
                <input type="text" 
                    wire:model.defer="name"
                    class="form-control border border-gray-300 focus:border-gray-600 placeholder-gray-800 border-gray-600" 
                    placeholder="Your name..."
                    required>
                @if($errors->has('name'))
                <span class="block mt-2 text-danger text-sm font-bold">{{ $errors->first('name') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="email" 
                    wire:model.defer="email"
                    class="form-control border border-gray-300 focus:border-gray-600 placeholder-gray-800 border-gray-600" 
                    placeholder="Your e-mail..."
                    required>
                @if($errors->has('email'))
                <span class="block mt-2 text-danger text-sm font-bold">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <button type="button" class="btn btn-danger" wire:loading.attr="disabled">
                Subscribe now!
            </button>
        </form>
    @else
        <p class="text-lg mb-3 text-gray-800 mt-7 font-bold uppercase">{{ $name }}!</p>
        <p class="text-gray-700 font-bold">Thank you for subscribing to our newsletter.</p>
    @endif
</div>
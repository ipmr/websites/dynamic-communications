<footer class="bg-white border-t border-gray-200 py-15">
    <div class="website-container hidden md:flex items-stretch">
        <div class="w-full text-center lg:text-left lg:w-2/3 flex flex-col">
            <div class="flex items-start mb-7 space-x-10">
                <div class="w-1/3">
                    <h2 class="text-lg font-bold uppercase mb-7">About us</h2>
                    <div class="flex flex-col space-y-2">
                        <a href="/about" class="footer-link">Company Overview</a>
                        <a href="/about#history" class="footer-link">History</a>
                        <a href="/about#presence" class="footer-link">Offices</a>
                        <a href="/about#partners" class="footer-link">Partners</a>
                        <a href="/#certifications" class="footer-link">Certifications</a>
                    </div>
                </div>
                <div class="w-1/3">
                    <h2 class="text-lg font-bold uppercase mb-7">Solutions</h2>
                    <div class="flex flex-col space-y-2">
                        <a href="{{ route('solutions.show', __('website.solutions.enterprise.route')) }}" class="footer-link">@lang('website.solutions.enterprise.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.data-center.route')) }}" class="footer-link">@lang('website.solutions.data-center.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.its.route')) }}" class="footer-link">@lang('website.solutions.its.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.software.route')) }}" class="footer-link">@lang('website.solutions.software.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.electronic.route')) }}" class="footer-link">@lang('website.solutions.electronic.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.cyber-security.route')) }}" class="footer-link">@lang('website.solutions.cyber-security.title')</a>
                        <a href="{{ route('solutions.show', __('website.solutions.access-and-wireless.route')) }}" class="footer-link">@lang('website.solutions.access-and-wireless.title')</a>
                    </div>
                </div>
                <div class="w-1/3">
                    <h2 class="text-lg font-bold uppercase mb-7">Services</h2>
                    <div class="flex flex-col space-y-2">
                        <a href="/services#it-consulting" class="footer-link">IT Consulting</a>
                        <a href="/services#maintenance" class="footer-link">Maintenance Contracts</a>
                        <a href="/services#network-assessment" class="footer-link">Network Assessment</a>
                        <a href="/services#it-equipment" class="footer-link">IT Equipment</a>
                        <a href="/services#help-desk" class="footer-link">Help Desk</a>
                    </div>
                </div>
            </div>
            <div class="mt-10 hidden lg:block">
                <p class="text-xs font-bold">
                    ©{{ now()->format('Y') }}, {{ config('app.name') }}, Rights Reserved. | <a href="{{ route('privacy') }}" class="text-danger hover:opacity-75">Privacy Policy</a>
                </p>
            </div>
        </div>
        <div class="w-1/3 hidden lg:block">
            @livewire('subscribe-form')  
        </div>
    </div>
    <div class="website-container flex m-0 md:mt-10 lg:hidden">
        <div class="w-full md:w-1/2 mx-auto text-left md:text-center">
            @livewire('subscribe-form')  
            <p class="text-xs font-bold mt-10">
            ©{{ now()->format('Y') }}, {{ config('app.name') }}, Rights Reserved. | <a href="{{ route('privacy') }}" class="text-danger hover:opacity-75">Privacy Policy</a>
            </p>
        </div>
    </div>
</footer>
<section class="py-10 lg:py-24">
    <div class="website-container block md:flex items-center space-x-0 md:space-x-7 lg:space-x-15">
        <div class="w-full md:w-1/2">
            <h1 class="h3 text-gray-400 wow animate__fadeInUp" data-wow-delay=".3s">OVERVIEW</h1>
            <p class="lead">
                For now more than {{ config('app.years_of_experience') }} years, <b>Dynamic Communications</b> has been an industry leader in Seamless International IP Telephony Applications, Data Networks and Structured Cabling Solutions. Our customers range from Small Businesses to Large Multinational Companies, across a variety of Vertical Markets.
            </p>
            <a href="{{ route('about') }}" class="btn btn-danger">Read more</a>
        </div>
        <div class="w-full md:w-1/2 mt-10 md:m-0">
            <div class="videoWrapper shadow-2xl overflow-hidden rounded">
            </div>
        </div>
    </div>
</section>

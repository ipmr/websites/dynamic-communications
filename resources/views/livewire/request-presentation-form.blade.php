<section class="bg-dark-900 py-10 lg:py-32 bg-overlay bg-fixed" :style="{backgroundImage: 'url(\'/img/unsplash/headway-5QgIuuBxKwM-unsplash.jpg\')'}">
    <div class="website-container">
        <div class="flex">
            <div class="mx-auto lg:m-0 lg:w-1/2">
                <h1 class="text-base md:text-xl mb-4 text-white">
                    Learn more about us, please request our:
                </h1>
                <h1 class="h3 uppercase wow animate__fadeInUp text-white leading-tight" data-wow-delay=".25s">
                    Products & Services Catalog
                </h1>

                <form action="">
                    @csrf
                    <div class="form-group text-left">
                        <label class="label text-white block" for="">Full name:</label>
                        <input type="text" 
                            class="form-control form-control-lg placeholder-gray-600" 
                            placeholder="Your name"
                            name="name"
                            required>
                        <p>
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </p>
                    </div>
                    <div class="form-group text-left">
                        <label class="label text-white block" for="">E-mail:</label>
                        <input type="email" 
                            class="form-control form-control-lg placeholder-gray-600" 
                            placeholder="your@email.com"
                            name="email"
                            required>
                        <p>
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </p>
                    </div>
                    <div class="form-group text-left">
                        <label class="label text-white flex" for="">
                            <span>Company name:</span>
                        </label>
                        <input type="text" 
                            class="form-control form-control-lg placeholder-gray-600" 
                            placeholder="Company name"
                            name="company"
                            required>
                        <p>
                            <small class="text-danger">{{ $errors->first('company') }}</small>
                        </p>
                    </div>
                    <button type="button" class="btn btn-danger mt-5 mx-auto">
                        Request Now
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
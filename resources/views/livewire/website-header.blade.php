<div>
    <header class="website-header">
        <div class="website-container py-4 md:py-2">

            <div class="hidden md:flex items-center">
                <div>
                    <p class="font-bold uppercase md:text-sm lg:text-base">
                        <i class="fa fa-phone mr-2"></i>
                        Call us: +52 (664) 622-7030
                    </p>
                </div>
                <div class="ml-auto flex items-center">
                    <p class="font-bold m-0 uppercase md:text-sm lg:text-base">
                        <a href="https://helpdesk.dynacom.services" target="_blank" class="link">
                            <i class="fa fa-headset mr-2"></i>
                            Customers - Service Ticket
                        </a>
                    </p>
                    <div class="ml-6">
                        <i class="fa fa-globe-americas text-gray-600 mr-2"></i>  
                        <a href="{{ url('locale/en') }}" class="font-bold @if(app()->getLocale() == 'en') text-danger @else text-gray-400 @endif">English</a> | 
                        <a href="{{ url('locale/es') }}" class="font-bold @if(app()->getLocale() == 'es') text-danger @else text-gray-400 @endif">Español</a>
                    </div>
                </div>
            </div>

            <hr class="hidden md:block border-gray-200 border-2 my-4">

            <div class="flex items-center">

                <a href="{{ route('home') }}" class="m-0">
                    <img src="/img/dynacom_logo.svg" class="website-header-logo" alt="">
                </a>
                
                <ul class="ml-auto hidden md:flex md:space-x-3 lg:space-x-10 website-header-nav">
                    <li><a href="{{ route('home') }}" class="{{request()->routeIs('home') ? 'active' : ''}}">Home</a></li>
                    <li><a href="{{ route('about') }}" class="{{request()->routeIs('about') ? 'active' : ''}}">About us</a></li>
                    <li>
                        <a class="{{request()->routeIs('solutions.*') ? 'active' : ''}}">Solutions<i class="fa fa-angle-down ml-3"></i></a>
                        <div class="subnav">
                            <div class="subnav-content">
                                <p class="subnav-title">@lang('website.solutions.our-solutions')</p>
                                <div>
                                    <a href="{{ route('solutions.show', __('website.solutions.enterprise.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.enterprise.icon')) }}" alt="">
                                        @lang('website.solutions.enterprise.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.data-center.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.data-center.icon')) }}" alt="">
                                        @lang('website.solutions.data-center.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.its.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.its.icon')) }}" alt="">
                                        @lang('website.solutions.its.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.software.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.software.icon')) }}" alt="">
                                        @lang('website.solutions.software.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.electronic.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.electronic.icon')) }}" alt="">
                                        @lang('website.solutions.electronic.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.cyber-security.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.cyber-security.icon')) }}" alt="">
                                        @lang('website.solutions.cyber-security.title')
                                    </a>
                                    <a href="{{ route('solutions.show', __('website.solutions.access-and-wireless.route')) }}">
                                        <img src="{{ asset('img/svg/' . __('website.solutions.access-and-wireless.icon')) }}" alt="">
                                        @lang('website.solutions.access-and-wireless.title')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="{{ route('services') }}">Services</a></li>
                    <li><a href="{{ route('contact') }}">Contact</a></li>
                </ul>

                <mobile-btn></mobile-btn>

            </div>
        </div>
    </header>
</div>
<section class="py-10 md:py-20" id="partners">
    <div class="website-container">
        <div class="flex mb-10">
            <div class="w-full md:w-1/2 mx-auto text-left md:text-center">
                <h1 class="h3 text-gray-400 wow animate__fadeInUp" data-wow-delay=".3s">Our partners</h1>
                <p class="text-xl">
                We strongly believe that good relationships are essential on establishing excellent quality services with the highest of the industry's standards, maintaining top notch customer service and support is our reason why we partner with the best IT companies in the world, and here are some examples:
                </p>
            </div>
        </div>

        <div class="flex">
            <div class="w-full lg:w-3/4 mx-auto">
                <div class="flex items-center md:justify-center flex-wrap space-x-0">
                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".3s">
                            <img src="/img/partners/avaya.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".6s">
                            <img src="/img/partners/cisco.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".9s">
                            <img src="/img/partners/belden.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay="1.2s">
                            <img src="/img/partners/commscope.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".3s">
                            <img src="/img/partners/hp.png" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".6s">
                            <img src="/img/partners/panduit.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".9s">
                            <img src="/img/partners/schneider.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

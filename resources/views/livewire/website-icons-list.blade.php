<section class="py-7 bg-white shadow">
    <div class="website-container flex justify-between space-x-4">
        <a href="{{ route('solutions.show', __('website.solutions.enterprise.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".2s">
            <img src="/img/svg/{{ __('website.solutions.enterprise.icon') }}" alt="" title="@lang('website.solutions.enterprise.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.data-center.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.data-center.icon') }}" alt="" title="@lang('website.solutions.data-center.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.its.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.its.icon') }}" alt="" title="@lang('website.solutions.its.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.software.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.software.icon') }}" alt="" title="@lang('website.solutions.software.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.electronic.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.electronic.icon') }}" alt="" title="@lang('website.solutions.electronic.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.cyber-security.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.cyber-security.icon') }}" alt="" title="@lang('website.solutions.cyber-security.title')">
        </a>
        <a href="{{ route('solutions.show', __('website.solutions.access-and-wireless.route')) }}" class="thumb-icon hvr-float-shadow wow animate__fadeIn" data-wow-delay=".4s">
            <img src="/img/svg/{{ __('website.solutions.access-and-wireless.icon') }}" alt="" title="@lang('website.solutions.access-and-wireless.title')">
        </a>
    </div>
</section>

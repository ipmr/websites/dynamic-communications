<section class="py-10 lg:py-15" id="certifications">
    <div class="website-container">
       
        <div class="flex mb-10">
            <div class="w-full md:w-1/2 mx-auto text-left md:text-center">
                <h1 class="h3 text-gray-400 wow animate__fadeInUp" data-wow-delay=".3s">
                    @lang('website.certifications.title')
                </h1>
                <p class="text-xl">
                    @lang('website.certifications.description')
                </p>
            </div>
        </div>

        <div class="flex">
            <div class="w-full lg:w-3/4 mx-auto">
                <div class="flex items-center md:justify-center flex-wrap space-x-0">
                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".3s">
                            <img src="/img/certifications/avayacert.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".6s">
                            <img src="/img/certifications/apccert.png" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".9s">
                            <img src="/img/certifications/bbbcert.png" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay="1.2s">
                            <img src="/img/certifications/ciscocert.png" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".3s">
                            <img src="/img/certifications/commscopecert.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".6s">
                            <img src="/img/certifications/hpcert.png" class="w-full cert-logo" alt="">
                        </div>
                    </div>

                    <div class="w-1/2 md:w-1/4 px-3 mb-5">
                        <div class="card-logo wow animate__zoomIn" data-wow-delay=".9s">
                            <img src="/img/certifications/panduitcert.jpg" class="w-full cert-logo" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

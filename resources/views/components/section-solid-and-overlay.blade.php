<section class="bg-danger w-full overflow-hidden">
    <div class="container px-0 lg:px-4 mx-auto block lg:flex items-stretch lg:space-x-28">
        <div class="flex-1 py-10 px-5 md:px-10 lg:px-0 lg:py-24">
            <h1 class="h3 text-white wow animate__fadeInLeft">{{ $title }}</h1>
            {{ $description }}
        </div>
        <div class="w-full lg:w-1/2 py-10 px-5 md:px-10 lg:px-0 lg:py-24 bg-dark-900 relative">
            <div class="bg-half bg-dark-900 bg-overlay" style="background-image: url('{{ $bgimg }}')"></div>
            <div class="relative z-10">
                {{ $bgdescription }}
            </div>
        </div>
    </div>
</section>
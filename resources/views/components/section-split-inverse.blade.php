<section class="py-10 lg:py-24">
    <div class="website-container block md:flex items-center md:space-x-15 lg:space-x-24">
        <div class="hidden md:block flex-1">
            <img src="{{ $pathimg }}" 
                class="wow animate__fadeInUp w-full h-auto shadow-xl rounded"
                data-wow-delay=".5s"
                alt="{{ $title }}">
        </div>
        <div class="w-full md:w-1/2">
            <h1 class="h3 wow animate__fadeInRight text-gray-400">{{ $title }}</h1>
            {{ $description }}
        </div>
        <div class="block md:hidden mt-10">
            <img src="{{ $pathimg }}" 
                class="wow animate__fadeInUp w-full h-auto shadow-xl rounded"
                data-wow-delay=".5s"
                alt="{{ $title }}">
        </div>
    </div>
</section>
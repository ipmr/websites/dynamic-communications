<section class="bg-dark-900 py-10 lg:py-48 bg-overlay md:bg-fixed" 
    style="background-image: url('{{ $pathimg }}')">
    <div class="website-container block lg:flex items-start lg:space-x-32">
        <h1 class="h3 wow animate__fadeInLeft text-white flex-1">{{ $title }}</h1>
        <div class="w-full lg:w-1/2 ml-auto">
            {{ $description }}
        </div>
    </div>
</section>
<section class="py-10 lg:py-24 bg-overlay" 
    style="background-image: url('{{ $bgimg }}')">
    <div class="container px-10 lg:px-4 mx-auto">
        <div class="flex">
            <div class="w-full md:w-2/3 mx-auto flex flex-col items-center">
                <img src="{{ $icon }}"
                    class="w-16 md:w-20 lg:w-24 h-auto mb-10 wow animate__fadeInUp" 
                    alt="{{ $title }}">
                <div class="text-center text-white">
                    <h1 class="slick-title uppercase">{{ $title }}</h1>
                    {{ $description }}
                </div>
            </div>
        </div>
    </div>
</section>
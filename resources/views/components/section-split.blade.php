<section class="py-10 lg:py-24 bg-white shadow-lg">
    <div class="website-container block md:flex items-center md:space-x-15 lg:space-x-24">
        <div class="w-full md:w-1/2">
            <h3 class="h3 wow animate__fadeInLeft text-gray-400" data-wow-delay=".5s">
                {{ $title }}
            </h3>
            {{ $description }}
        </div>
        <div class="w-full mt-10 md:m-0 md:w-1/2">
            <img src="{{ $pathimg }}"
                class="wow animate__fadeInUp w-full h-auto shadow-xl rounded"
                alt="{{ $title }}">
        </div>
    </div>
</section>
<div class="flex flex-col md:flex-row items-center md:items-center justify-center space-y-5 md:space-y-0 md:space-x-10 mt-10">
    @foreach($logos as $logo)
    <img src="/img/logos/{{ $logo }}" class="w-24 md:w-32 cert-logo" alt="">
    @endforeach
</div>